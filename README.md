# Water Watch

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

> Water Watch is an Ionic application to help keep track of ones water intake.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Features](#features)
- [Maintainers](#maintainers)
- [Contribute](#contribute)
- [License](#license)

## Install

```bash
$ script/bootstrap
```

```bash
$ npm install -g add-cors-to-couchdb
$ add-cors-to-couchdb
```

## Usage

```bash
$ script/server
```

## Features

- Set target water intake

## Maintainers

[@Dshoe](https://github.com/Dshoe)

## Contribute

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2018 Devin Shoemaker
