import { Component } from '@angular/core';
import { AlertController, NavController, ToastController } from 'ionic-angular';
import { WaterLogServiceProvider } from '../../providers/water-log-service/water-log-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  waterLog: WaterLog = {
    targetAmount: null,
    currentAmount: null,
    drinks: []
  };

  constructor(public navCtrl: NavController, private alertCtrl: AlertController, private toastCtrl: ToastController,
              private waterlogSvc: WaterLogServiceProvider) {

  }

  ionViewDidLoad() {
    this.getWaterLog();
  }

  getWaterLog() {
    this.waterlogSvc.getAllWaterLogs().then((data) => {
      if (data && data.length > 0) {
        this.waterLog = data[0];
      } else {
        this.promptTargetAmount();
      }
    });
  }

  promptTargetAmount() {
    let alert = this.alertCtrl.create({
      title: 'Create water log',
      message: 'Enter the amount of water you would like to drink each day.',
      inputs: [
        {
          name: 'targetAmount',
          placeholder: 'Target amount (fl oz)',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'Okay',
          handler: data => {
            if (data.targetAmount && data.targetAmount > 0) {
              this.waterLog.targetAmount = data.targetAmount;
              this.waterlogSvc.createWaterLog(this.waterLog);
              return true;
            } else {
              let toast = this.toastCtrl.create({
                message: 'Please enter a target amount greater than 0 fl oz.',
                duration: 2000,
                position: 'bottom'
              });
              toast.present();

              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

}
