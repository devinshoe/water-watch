/**
 * An instance of a single days water consumption.
 *
 * @author Devin Shoemaker (dshoemaker@gmail.com)
 */
class WaterLog {

  targetAmount: number;
  currentAmount: number;
  drinks: Drink[];

}
