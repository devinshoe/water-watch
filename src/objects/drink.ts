/**
 * An instance of a single drink consumed.
 *
 * @author Devin Shoemaker (devinshoe@gmail.com)
 */
class Drink {

  amount: number;
  date: Date;

}
