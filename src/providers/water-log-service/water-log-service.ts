import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';

/**
 * Read and write water log data to PouchDB and sync to remote CouchDB instance.
 *
 * @author Devin Shoemaker (devinshoe@gmail.com)
 */
@Injectable()
export class WaterLogServiceProvider {

  data: any;
  db: any;
  remote: any;

  constructor() {
    this.db = new PouchDB('water_watch');

    this.remote = 'http://127.0.0.1:5984/water_watch';

    let options = {
      live: true,
      retry: true,
      continuous: true
    };

    this.db.sync(this.remote, options);
  }

  /**
   * Get all water logs from PouchDB.
   *
   * @returns {Promise<any>} Promise to retrieve and store water logs.
   */
  getAllWaterLogs() {
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.db.allDocs({
        include_docs: true
      }).then((result) => {
        this.data = [];

        result.rows.map((row) => {
          this.data.push(row.doc);
        });

        resolve(this.data);

        this.db.changes({live: true, since: 'now', include_docs:true}).on('change', (change) => {
          this.handleChange(change);
        });
      }).catch((error) => {
        console.log(error);
      });
    });
  }

  /**
   * Create and store a new water log.
   *
   * @param waterLog A new water log to be saved.
   */
  createWaterLog(waterLog: WaterLog) {
    this.db.post(waterLog);
  }

  /**
   * Update an existing water log.
   *
   * @param waterLog An updated water log to be saved.
   */
  updateWaterLog(waterLog: WaterLog) {
    this.db.put(waterLog).catch((err) => {
      console.log(err);
    });
  }

  /**
   * Delete an existing water log.
   *
   * @param waterLog A water log to be deleted.
   */
  deleteWaterLog(waterLog: WaterLog) {
    this.db.remove(waterLog).catch((err) => {
      console.log(err);
    });
  }

  /**
   * Update local list of water logs if a change in the PouchDB instance is detected.
   *
   * @param change The modified transaction.
   */
  handleChange(change) {
    let changedDoc = null;
    let changedIndex = null;

    this.data.forEach((doc, index) => {
      if (doc._id === change.id) {
        changedDoc = doc;
        changedIndex = index;
      }
    });

    // A document was deleted
    if (change.deleted) {
      this.data.splice(changedIndex, 1);
    } else {
      // A document was updated or added
      if (changedDoc) {
        this.data[changedIndex] = change.doc;
      } else {
        this.data.push(change.doc);
      }
    }
  }

}
